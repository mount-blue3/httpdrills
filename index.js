const http = require("http");
const { v4: uuidv4 } = require("uuid");
const server = http.createServer((req, res) => {
  if (req.method === "GET" && req.url === "/html") {
    res.writeHead(200, { "Content-Type": "text/html" });
    res.write(`<!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
              <p> - Martin Fowler</p>
          </body>
        </html>`);
    res.end();
  } else if (req.method === "GET" && req.url === "/json") {
    res.writeHead(200, { "Content-Type": "application/json" });
    res.write(`{
        "slideshow": {
          "author": "Yours Truly",
          "date": "date of publication",
          "slides": [
            {
              "title": "Wake up to WonderWidgets!",
              "type": "all"
            },
            {
              "items": [
                "Why <em>WonderWidgets</em> are great",
                "Who <em>buys</em> WonderWidgets"
              ],
              "title": "Overview",
              "type": "all"
            }
          ],
          "title": "Sample Slide Show"
        }
      }`);
    res.end();
  } else if (req.method === "GET" && req.url === "/uuid") {
    res.writeHead(200, { "Content-Type": "application/json" });
    res.write(JSON.stringify({ uuid: uuidv4() }));
    res.end();
  } else if (req.method === "GET" && req.url.startsWith("/status/")) {
    const statusCode = parseInt(req.url.split("/").pop());
    res.writeHead(statusCode, { "Content-Type": "application/json" });
    res.end(`Status code: ${statusCode}`);
  } else if (req.method === "GET" && req.url.startsWith("/delay/")) {
    const delayInSeconds = parseInt(req.url.split("/").pop());
    res.writeHead(200, { "Content-Type": "text/plain" });
    setTimeout(() => {
      res.write(`Response is coming after ${delayInSeconds} seconds`);
      res.end();
    }, delayInSeconds * 1000);
  } else {
    res.writeHead(404, { "Content-Type": "text/plain" });
    res.end("NOT FOUND");
  }
});
server.listen(7000, () => {
  console.log("Server running on port 7000");
});
